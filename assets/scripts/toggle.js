function navtoggler() {
  var nav = document.getElementById("nav");
  var toggle = document.getElementById("toggle");
  if (nav.className === "" && toggle.className === "") {
    nav.className += "navMenuResponsive";
    toggle.className += "on";
  } else {
    nav.className = "";
    toggle.className = "";
  }
}

function planDurationSwitch(a) {
  if (a.checked == true) { 
    document.getElementById("apiPlansMonthly").style.display = "none"; 
    document.getElementById("apiPlansYearly").style.display = "block";
  }
  else { 
    document.getElementById("apiPlansYearly").style.display = "none"; 
    document.getElementById("apiPlansMonthly").style.display = "block"; 
  }
}