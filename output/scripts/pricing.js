let domainData;
async function fetchText() {
    let domainLink = await fetch('https://billing.ipgeolocation.io/plan');
    let data = await domainLink.text();
    domainData = JSON.parse(data);


    document.getElementById("devPnameMonthly").innerHTML = domainData[0].name;

    document.getElementById("bronze_nameMonthly").innerHTML = domainData[1].name;
    document.getElementById("bronze_rateMonthly").innerHTML = "$"+domainData[1].rate;
    document.getElementById("bronze_requestsLimitMonthly").innerHTML = domainData[1].planApiUsageLimit.requests/1000+"K requests per month";
    document.getElementById("bronze_surchargeRateMonthly").innerHTML = "$"+domainData[1].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[1].planApiUsageLimit.surchargeRequests/1000+"K Requests";
    
    document.getElementById("silver_nameMonthly").innerHTML = domainData[3].name;
    document.getElementById("silver_rateMonthly").innerHTML = "$"+domainData[3].rate;
    document.getElementById("silver_requestsLimitMonthly").innerHTML = domainData[3].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("silver_surchargeRateMonthly").innerHTML = "$"+domainData[3].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[3].planApiUsageLimit.surchargeRequests/1000+"K Requests";

    document.getElementById("silverPMonthly").innerHTML = domainData[4].name;
    document.getElementById("silverP_rateMonthly").innerHTML = "$"+domainData[4].rate;
    document.getElementById("silverP_requestsLimitMonthly").innerHTML = domainData[4].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("silverP_surchargeRateMonthly").innerHTML = "$"+domainData[4].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[4].planApiUsageLimit.surchargeRequests/1000+"K Requests";

    document.getElementById("gold_nameMonthly").innerHTML = domainData[5].name;
    document.getElementById("gold_rateMonthly").innerHTML = "$"+domainData[5].rate;
    document.getElementById("gold_requestsLimitMonthly").innerHTML = domainData[5].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("gold_surchargeRateMonthly").innerHTML = "$"+domainData[5].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[5].planApiUsageLimit.surchargeRequests/1000+"K Requests";

    document.getElementById("platinum_nameMonthly").innerHTML = domainData[6].name;
    document.getElementById("platinum_rateMonthly").innerHTML = "$"+domainData[6].rate;
    document.getElementById("platinum_requestsLimitMonthly").innerHTML = domainData[6].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("platinum_surchargeRateMonthly").innerHTML = "$"+domainData[6].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[6].planApiUsageLimit.surchargeRequests/1000+"K Requests";


    document.getElementById("bronze_nameYearly").innerHTML = domainData[9].name;
    document.getElementById("bronze_rateYearly").innerHTML.replace("$150","$"+domainData[9].rate);
    document.getElementById("bronze_requestsLimitYearly").innerHTML = domainData[9].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("bronze_surchargeRateYearly").innerHTML = "$"+domainData[9].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[9].planApiUsageLimit.surchargeRequests/1000+"K Requests";
    
    document.getElementById("silver_nameYearly").innerHTML = domainData[11].name;
    document.getElementById("silver_rateYearly").innerHTML.replace("$650","$"+domainData[11].rate);
    document.getElementById("silver_requestsLimitYearly").innerHTML = domainData[11].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("silver_surchargeRateYearly").innerHTML = "$"+domainData[11].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[11].planApiUsageLimit.surchargeRequests/1000+"K Requests";

    document.getElementById("silverPYearly").innerHTML = domainData[12].name;
    document.getElementById("silverP_rateYearly").innerHTML.replace("$1300","$"+domainData[12].rate);
    document.getElementById("silverP_requestsLimitYearly").innerHTML = domainData[12].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("silverP_surchargeRateYearly").innerHTML = "$"+domainData[12].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[12].planApiUsageLimit.surchargeRequests/1000+"K Requests";

    document.getElementById("gold_nameYearly").innerHTML = domainData[13].name;
    document.getElementById("gold_rateYearly").innerHTML.replace("$2000","$"+domainData[13].rate);
    document.getElementById("gold_requestsLimitYearly").innerHTML = domainData[13].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("gold_surchargeRateYearly").innerHTML = "$"+domainData[13].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[13].planApiUsageLimit.surchargeRequests/1000+"K Requests";

    document.getElementById("platinum_nameYearly").innerHTML = domainData[14].name;
    document.getElementById("platinum_rateYearly").innerHTML.replace("$5000","$"+domainData[14].rate);
    document.getElementById("platinum_requestsLimitYearly").innerHTML = domainData[14].planApiUsageLimit.requests/1000000+"M requests per month";
    document.getElementById("platinum_surchargeRateYearly").innerHTML = "$"+domainData[14].planApiUsageLimit.surchargeRate+" Per Extra "+domainData[14].planApiUsageLimit.surchargeRequests/1000+"K Requests";
}
fetchText();